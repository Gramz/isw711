const express = require('express');
const mongoose = require("mongoose");
const db = mongoose.connect("mongodb://127.0.0.1:27017/course-api");
const app = express();
const cors = require("cors");
const bodyParser = require("body-parser");

const {
    courseGet,
    coursePost,
    coursePatch,
    courseDelete
} = require("./controllers/courseController.js");

const {
    saveSession,
    getSession,
} = require("./controllers/sessionController.js");

app.use(cors());

app.use(
    bodyParser.urlencoded({
        extended: true
    })
);
app.use(bodyParser.json());

app.post("/api/session", function (req, res, next) {
    if (req.body.username && req.body.password &&
        req.body.username === 'admin' && req.body.password === 'password') {
        const session = saveSession(req.body.username);
        session.then(function (session
        ) {
            if (!session) {
                res.status(422);
                res.json({
                    error: 'There was an error saving the session'
                });
            }
            res.status(201).json({
                session
            });
        })
    } else {
        res.status(422);
        res.json({
            error: 'Invalid username or password'
        });
    }
});

app.use(function (req, res, next) {
    if (req.headers["authorization"]) {
        const token = req.headers['authorization'].split(' ')[1];
        try {
            const session = getSession(token);
            session.then(function (session) {
                if (session) {
                    next();
                    return;
                } else {
                    res.status(401);
                    res.send({
                        error: "Unauthorized"
                    });
                }
            })
                .catch(function (err) {
                    console.log('there was an error getting the session', err);
                    res.status(422);
                    res.send({
                        error: "There was an error: " + err.message
                    });
                })
        } catch (e) {
            res.status(422);
            res.send({
                error: "There was an error: " + e.message
            });
        }
    } else {
        res.status(401);
        res.send({
            error: "Unauthorized"
        });
    }
});

app.get("/api/courses", courseGet);
app.post("/api/courses", coursePost);
app.patch("/api/courses", coursePatch);
app.put("/api/courses", coursePatch);
app.delete("/api/courses", courseDelete);


app.listen(3000, () => console.log(`Course app is listening on port 3000!`))