const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const course = new Schema({
    name: {type: String},
    code: {type: String},
    career: {type: String},
    numberOfCredits: {type: Number}
});

module.exports = mongoose.model('courses', course);