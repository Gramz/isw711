const Course = require("../models/courseModel");

const coursePost = (req, res) => {
    let course = new Course();

    course.name = req.body.name;
    course.code = req.body.code;
    course.career = req.body.career;
    course.numberOfCredits = req.body.numberOfCredits;

    if (course.name && course.code && course.career && course.numberOfCredits) {
        course.save(function (err) {
            if (err) {
                res.status(422);
                console.log('error while saving the course', err)
                res.json({
                    error: 'There was an error saving the course'
                });
            }
            res.status(201);
            res.header({
                'location': `http://localhost:3000/api/courses/?id=${course.id}`
            });
            res.json(course);
        });
    } else {
        res.status(422);
        console.log('error while saving the course')
        res.json({
            error: 'No valid data provided for course'
        });
    }
};

const courseGet = (req, res) => {
    if(req.query && req.query.id){
        Course.findById(req.query.id, function (err, course) {
            if (err) {
                res.status(404);
                console.log('error while queryting the course', err)
                res.json({
                    error: 'Course doesnt exist'
                })
            }
            res.json(course);
        });
    } else {
        Course.find(function (err, courses) {
            if(err){
                res.status(422);
                res.json({"error": err});
            }
            res.json(courses);
        });
    }
};

const coursePatch = (req, res) => {
    if (req.query && req.query.id) {
        Course.findById(req.query.id, function (err, course) {
            if (err) {
                res.status(404);
                console.log('error while queryting the course', err)
                res.json({
                    error: "Course doesnt exist"
                })
            }

            course.name = req.body.name ? req.body.name : course.name;
            course.code = req.body.code ? req.body.code : course.code;
            course.career = req.body.career ? req.body.career : course.career;
            course.numberOfCredits = req.body.numberOfCredits ? req.body.numberOfCredits : course.numberOfCredits;

            course.save(function (err) {
                if (err) {
                    res.status(422);
                    console.log('error while saving the course', err)
                    res.json({
                        error: 'There was an error saving the course'
                    });
                }
                res.status(200);
                res.json(course);
            });
        });
    } else {
        res.status(404);
        res.json({
            error: "Course doesnt exist"
        })
    }
};

const courseDelete = (req, res) => {
    if (req.query && req.query.id) {
        Course.findById(req.query.id, function (err, course) {
            if (err) {
                res.status(500);
                console.log('error while queryting the course', err)
                res.json({
                    error: "Course doesnt exist"
                })
            }
            if (course) {
                course.remove(function (err) {
                    if (err) {
                        res.status(500).json({
                            message: "There was an error deleting the Course"
                        });
                    }
                    res.status(204).json({});
                })
            } else {
                res.status(404);
                console.log('error while queryting the course', err)
                res.json({
                    error: "Course doesnt exist"
                })
            }
        });
    } else {
        res.status(404).json({
            error: "You must provide a Course ID"
        });
    }
};

module.exports = {
    coursePost,
    courseGet,
    coursePatch,
    courseDelete
}