const express = require('express');
const app = express();
const cors = require("cors");
const mongoose = require("mongoose");
const db = mongoose.connect("mongodb://127.0.0.1:27017/course-api");
const bodyParser = require("body-parser");

const {
    courseGet,
    coursePost,
    coursePatch,
    courseDelete
} = require("./controllers/courseController.js");

app.use(bodyParser.json());

//check for cors
app.use(cors({
    domains: '*',
    methods: "*"
}));

app.get("/api/courses",courseGet);
app.post("/api/courses",coursePost);
app.patch("/api/courses",coursePatch);
app.put("/api/courses",coursePatch);
app.delete("/api/courses",courseDelete);




app.listen(3000, () => console.log(`Example app listening on port 3000!`))